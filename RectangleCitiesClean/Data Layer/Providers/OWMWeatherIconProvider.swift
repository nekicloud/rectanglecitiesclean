//
//  OWMWeatherIconProvider.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class OWMWeatherIconProvider: WeatherIconProvider{
    let webService: WebService
    var iconCache: [String: Data] = [:]
    
    init(webService: WebService = WebServiceProvider(session: DataNetworkSession())){
        self.webService = webService
    }
    
    func fetchIcon(for path: String, completion: @escaping (Response<Data>) -> Void) {
        if let iconData = iconCache[path] {
            completion(.success(iconData))
        }
        guard let imageRequest = WeatherIconRequest(path: path).urlRequest else{ return}
        webService.execute(imageRequest) { [weak self] (response: Response<Data>) in
            switch response{
            case .success(let imageData):
                self?.iconCache[path] = imageData
                completion(.success(imageData))
            case .error(let error):
                print(error)
            }
        }
    }
}
