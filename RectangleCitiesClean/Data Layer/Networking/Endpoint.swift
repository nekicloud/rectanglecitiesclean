//
//  Endpoint.swift
//  RectangleCitiesClean
//
//  Created by Luka on 04/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Endpoint {
    case boundingBox(boundingBox : BoundingBoxCoordinates)
    
    var parameters: String {
        switch self{
        case let .boundingBox(boundingBox : BoundingBoxCoordinates):
            return "?bbox=\(BoundingBoxCoordinates.lonLeft),\(BoundingBoxCoordinates.latBottom),\(BoundingBoxCoordinates.lonRight),\(BoundingBoxCoordinates.latTop),\(BoundingBoxCoordinates.zoom)"
        }
    }
}
