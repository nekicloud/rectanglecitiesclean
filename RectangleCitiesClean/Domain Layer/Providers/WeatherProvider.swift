//
//  CityProvider.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol WeatherProvider{
    func fetchWeather(for coordinates: BoundingBoxCoordinates, completion: @escaping(Response<WeatherArray>) -> Void)
}
