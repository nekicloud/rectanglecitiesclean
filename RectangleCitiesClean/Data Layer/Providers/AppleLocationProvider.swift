//
//  AppleLocationProvider.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import CoreLocation

class AppleLocationProvider: LocationProvider {
    var locationManager = CLLocationManager()
    
    func requestLocation(){
        startUpdating()
        if !CLLocationManager.locationServicesEnabled(){
            print("LOCATION NOT AUTHORIZED")
        }
    }
    
    private func startUpdating(){
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
    }
}
