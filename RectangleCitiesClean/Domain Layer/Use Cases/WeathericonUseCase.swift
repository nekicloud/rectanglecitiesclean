//
//  WeathericonUseCase.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol WeatherIconUseCase {
    func fetchIcon(for path: String, completion: @escaping(Response<WeatherIcon>) -> Void)
}
