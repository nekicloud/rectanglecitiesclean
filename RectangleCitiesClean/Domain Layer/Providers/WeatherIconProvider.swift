//
//  CoordinatesProvider.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import MapKit

protocol WeatherIconProvider {
    func fetchIcon(for path: String, completion: @escaping (Response<Data>) -> Void)
}
