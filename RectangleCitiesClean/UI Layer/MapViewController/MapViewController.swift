//
//  MapViewController.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController{
    @IBOutlet weak var mapView: MKMapView!
    let dataSource:MapViewControllerDataSource
    
    init(dataSource: MapViewControllerDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setUpMapView()
        dataSource.fetchWeather(boundingBox: mapView.getBoundingBox(), mapView: mapView)
    }
    
    private func setUpMapView(){
        mapView.delegate = dataSource
    }
}

extension MKMapView {
    func getBoundingBox() -> BoundingBoxCoordinates{
        let topLeft: CLLocationCoordinate2D = convert(CGPoint(x: bounds.origin.x,y: bounds.maxY), toCoordinateFrom: self)
        let bottomRight: CLLocationCoordinate2D = convert(CGPoint(x: bounds.maxX, y: bounds.origin.y), toCoordinateFrom: self)
        return BoundingBoxCoordinates(lonLeft: topLeft.longitude, latBottom: bottomRight.latitude, lonRight: bottomRight.longitude, latTop: topLeft.latitude)
    }
}
