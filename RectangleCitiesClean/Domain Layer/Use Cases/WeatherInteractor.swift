//
//  WeatherInteractor.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class WeatherInteractor: WeatherUseCase{
    let provider: WeatherProvider
    
    init(provider: WeatherProvider) {
        self.provider = provider
    }
    func fetchWeather(for bbox: BoundingBoxCoordinates, completion: @escaping (Response<WeatherArray>) -> Void) {
        provider.fetchWeather(for: bbox, completion: completion)
    }
}
