//
//  WeatherIcon.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct WeatherIcon{
    var path: String
    var image: Data
    
    init(path: String, image: Data){
        self.path = path
        self.image = image
    }
}
