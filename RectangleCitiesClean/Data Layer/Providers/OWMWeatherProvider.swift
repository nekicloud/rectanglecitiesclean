//
//  OWMCityProvider.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class OWMWeatherProvider: WeatherProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworkSession())){
        self.webService = webService
    }
    
    func fetchWeather(for coordinates: BoundingBoxCoordinates, completion: @escaping (Response<WeatherArray>) -> Void) {
        guard let request = ApiRequest(endpoint: .boundingBox(boundingBox: coordinates)).urlRequest else {return}
        webService.execute(request) { (response: Response<Result>) in
            switch response{
            case .success(let weatherArray):
                completion(.success(formWeatherArray(result: weatherArray)))
            case .error(let error):
                print(request)
                print(error)
            }
        }
    }
}

private func formWeatherArray(result: Result) -> WeatherArray{
    var weatherArray = WeatherArray()
    for object in result.list{
        let nextWeather = OWMWeather(cityName: object.cityName, temperature: object.main.temp, longitude: object.coordinates.lon, latitude: object.coordinates.lat
            ,iconPath: object.weatherIcon, description: object.weatherDescription)
        weatherArray.append(nextWeather)
    }
    return weatherArray
}

struct OWMWeather: Weather, Decodable{
    var temperature: Double
    var longitude: Double
    var latitude: Double
    var iconPath: String
    var cityName: String
    var description: String
    
    init(cityName: String, temperature: Double, longitude: Double, latitude: Double, iconPath: String, description: String){
        self.cityName = cityName
        self.temperature = temperature
        self.longitude = longitude
        self.latitude = latitude
        self.iconPath = iconPath
        self.description = description
    }
}

struct Result: Decodable {
    enum CodingKeys: String, CodingKey{
        case list
    }
    let list: [List]
}

struct List: Decodable {
    enum CodingKeys: String, CodingKey{
        case cityName = "name"
        case coordinates = "coord"
        case main, weather
    }
    let cityName: String
    let main: Main
    let weather: [JSONWeather]
    var weatherIcon: String {
        guard let icon = weather.first?.icon else{ return ""}
        return icon
    }
    var weatherDescription: String {
        guard let description = weather.first?.description else { return ""}
        return description
    }
    let coordinates: Coordinates
}

struct Coordinates: Decodable {
    enum CodingKeys: String, CodingKey{
        case lon = "Lon"
        case lat = "Lat"
    }
    let lon: Double
    let lat: Double
}

struct JSONWeather: Decodable{
    let description: String
    let icon: String
}

struct Main: Decodable {
    enum CodingKeys: String, CodingKey{
        case temp
    }
    let temp: Double
}
