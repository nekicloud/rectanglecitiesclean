//
//  Coordinates.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct BoundingBoxCoordinates {
    var lonLeft: Double
    var latBottom: Double
    var lonRight: Double
    var latTop: Double
    var zoom: Int

    init(lonLeft: Double, latBottom: Double, lonRight: Double, latTop: Double){
        self.lonLeft = lonLeft
        self.latBottom = latBottom
        self.lonRight = lonRight
        self.latTop = latTop
        self.zoom = 10
    }
}
