//
//  CitiesInteractor.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class CitiesInteractor: CitiesUseCase{
    let provider: WeatherProvider
    
    init(provider: WeatherProvider){
        self.provider = provider
    }
    
    func fetchCities(lonLeft: Double, latBottom: Double, lonRight: Double, latTop: Double, zoom: Int, completion: @escaping (Response<WeatherArray>) -> Void) {
        provider.fetchCities(lonLeft: lonLeft, latBottom: latBottom, lonRight: lonRight, latTop: latTop, zoom: zoom, completion: completion)
    }
}
