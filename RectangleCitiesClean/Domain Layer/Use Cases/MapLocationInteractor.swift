//
//  CoordinatesInteractor.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation
import MapKit

class MapLocationInteractor: MapLocationUseCase {
    let provider:WeatherIconProvider
    
    init(provider: WeatherIconProvider){
        self.provider = provider
    }
    
    func fetchCoordinates(mapView: MKMapView) -> BoundingBoxCoordinates? {
        return provider.fetchIcon(mapView: mapView)
    }
}

