//
//  CoordinatesUseCase.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import MapKit

protocol MapLocationUseCase {
    func fetchCoordinates(mapView: MKMapView) -> BoundingBoxCoordinates?
}
