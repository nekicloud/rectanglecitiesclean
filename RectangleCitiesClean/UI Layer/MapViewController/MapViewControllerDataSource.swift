//
//  MapVCDataSource.swift
//  RectangleCitiesClean
//
//  Created by Luka on 11/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDependencies {
    var weatherUseCase: WeatherUseCase {get}
    var iconUseCase: WeatherIconUseCase {get}
    var locationUseCase: LocationUseCase {get}
}

class MapViewControllerDataSource: NSObject{
    private let dependencies: MapViewControllerDependencies
    private var annotationImages = [String:UIImage]()
    private var mapView: MKMapView?
    
    init(dependencies: MapViewControllerDependencies){
        self.dependencies = dependencies
        dependencies.locationUseCase.requestLocation()
    }
    
    func fetchWeather(boundingBox: BoundingBoxCoordinates, mapView: MKMapView){
        dependencies.weatherUseCase.fetchWeather(for: boundingBox) { [weak self] resp in
            switch resp {
            case .success(let weatherArray):
                self?.mapView = mapView
                self?.fetchPins(weatherArray: weatherArray)
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchPins(weatherArray: WeatherArray) {
        for object in weatherArray{
            let coordinates = CLLocationCoordinate2D(latitude: object.latitude, longitude: object.longitude)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinates
            annotation.title = "\(object.temperature)" + "°C | " + object.cityName
            annotation.subtitle = object.description
            fetchIcon(weather: object, annotation: annotation)
        }
    }
    
    private func fetchIcon(weather: Weather, annotation: MKPointAnnotation){
        dependencies.iconUseCase.fetchIcon(for: weather.iconPath) {[weak self] response in
            switch response {
            case .success(let icon):
                let image = UIImage(data: icon.image)
                if let mapView = self?.mapView{
                    self?.annotationImages[weather.description] = image
                    mapView.addAnnotation(annotation)
                }
            case .error(let error):
                print(error.localizedDescription)
                
            }
        }
    }
    
}

extension MapViewControllerDataSource: MKMapViewDelegate{
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        fetchWeather(boundingBox: mapView.getBoundingBox(), mapView: mapView)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "myPin"
        var annotationView: MKAnnotationView?
        
        if let dequeued = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) {
            annotationView = dequeued
        }
        else {
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView = av
        }
        annotationView?.canShowCallout = true
        guard let key = annotation.subtitle! else { return nil}
        annotationView?.image = annotationImages[key]
        return annotationView
    }
}

