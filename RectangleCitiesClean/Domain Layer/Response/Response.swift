//
//  Response.swift
//  RectangleCitiesClean
//
//  Created by Luka on 04/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Response<T> {
    case success(T)
    case error(Error)
}
