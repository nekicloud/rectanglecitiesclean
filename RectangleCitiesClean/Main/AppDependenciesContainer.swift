//
//  AppDependenceisContainer.swift
//  RectangleCitiesClean
//
//  Created by Luka on 11/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class AppDependenciesContainer: MapViewControllerDependencies{
    var weatherUseCase: WeatherUseCase = WeatherInteractor(provider: OWMWeatherProvider())
    var iconUseCase: WeatherIconUseCase = WeatherIconInteractor(provider: OWMWeatherIconProvider())
    var locationUseCase: LocationUseCase = LocationInteractor(provider: AppleLocationProvider())
}
