//
//  City.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol Weather {
    var cityName: String { get }
    var temperature: Double { get }
    var longitude: Double { get }
    var latitude: Double { get }
    var iconPath: String { get }
    var description: String { get}
}

typealias WeatherArray = [Weather]
