//
//  LocationInteractor.swift
//  RectangleCitiesClean
//
//  Created by Luka on 12/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class LocationInteractor: LocationUseCase {
    let provider:LocationProvider
    
    init(provider: LocationProvider){
        self.provider = provider
    }
    
    func requestLocation() {
        provider.requestLocation()
    }
}
