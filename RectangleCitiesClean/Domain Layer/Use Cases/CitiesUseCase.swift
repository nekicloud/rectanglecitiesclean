//
//  CityUseCase.swift
//  RectangleCitiesClean
//
//  Created by Luka on 07/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol CitiesUseCase{
    func fetchCities(lonLeft: Double, latBottom: Double, lonRight: Double, latTop: Double, zoom: Int, completion: @escaping(Response<WeatherArray>) -> Void)
}
