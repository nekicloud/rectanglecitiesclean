//
//  AppCoordinator.swift
//  RectangleCitiesClean
//
//  Created by Luka on 04/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AppCoordinator {
    let window: UIWindow
    let mapVC: MapViewController
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds), dependencies: AppDependenciesContainer){
        self.window = window
        self.mapVC = MapViewController(dataSource: MapViewControllerDataSource(dependencies: dependencies))
    }
    
    func start(){
        window.rootViewController = mapVC
        window.makeKeyAndVisible()
    }
}
